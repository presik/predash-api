# Multiple tools for dash api
# import json
import os
import configparser
from flask import jsonify
from Crypto.Cipher import XOR
import base64

# host_ = config.get('General', 'host')
# API_KEY = config.get('Auth', 'api_key')
# SECRET_KEY = config.get('Auth', 'secret_key')
# USER = config.get('Auth', 'user')
# active_ssl = False
# if 'SSL' in config.sections():
#     active_ssl = True
#     CERT_FILE = config.get('SSL', 'cert_file')
#     KEY_FILE = config.get('SSL', 'key_file')


def get_config(param=None):
    default_dir = os.path.join(os.getenv('HOME'), '.flask')
    config_file = os.path.join(default_dir, 'dash.ini')

    config = configparser.ConfigParser()
    config.read(config_file)
    if param:
        return config.get('General', param)
    return config


def encrypt(key, plaintext):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))


def send_data(data):
    res = jsonify(data)
    res.headers.add('Access-Control-Allow-Origin', '*')
    res.headers.add('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS')
    res.headers.add('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token')
    return res


def set_view_attr(field, args, named=True, target_model=None):
    view_dict = {}
    for arg in args:
        value = getattr(field, arg)
        if arg == 'depends' and value:
            value = value.replace(' ', '')
            value = value.split(',')
        if value == 'selection':
            selection_field = getattr(target_model, field.name)
            if selection_field:
                data = []
                for k, v in selection_field.selection:
                    data.append({
                        'key': k,
                        'value': k,
                        'text': v,
                    })
                view_dict['data_source'] = data

        if not value:
            continue

        view_dict[arg] = value

        # For one2many
        if value == 'one2many' and arg == 'type':
            view_dict['ctx_view'] = prepare_ctx_view(field.submodel)
    if named:
        return {field.name: view_dict}
    else:
        return view_dict


def get_webfields(dash_model, target_model=None):
    res = {}
    args = ['type', 'required', 'readonly', 'visible', 'model', 'translate',
        'parent_field', 'msg_ok', 'default', 'domain', 'redirect', 'depends',
        'data_source', 'prefix', 'suffix', 'trigger_function', 'searchable']

    required_fields = []
    for wf in dash_model.webfields:
        res.update(set_view_attr(wf, args, target_model=target_model))
        if not getattr(wf, 'readonly') and getattr(wf, 'required'):
            required_fields.append(wf.name)

    return res, required_fields


def get_webuserfom(fields):
    res = []
    args = ['name', 'component', 'expand', 'img_link']
    for wf in fields:
        res.append(set_view_attr(wf, args, named=False))
    return res


def prepare_ctx_view(model, target_model=None):
    data_view = {
        'name': model.model.model,
        'order': model.order,
        'order_by': model.order_by,
        'icon': model.icon,
        'calendar': model.calendar,
        'sheet': model.sheet,
        'start_view': model.start_view,
        'form_action_add': model.form_action_add,
        'form_action_save': model.form_action_save,
        'table_action_update': model.table_action_update,
        'table_action_remove': model.table_action_remove,
        'webfields': {},
        'webform': [],
        'webtree': []
    }

    webfields, required = get_webfields(model, target_model)
    data_view['webfields'] = webfields
    data_view['required'] = required

    args = ['name', 'component', 'expand']
    for wf in model.webform:
        data_view['webform'].append(set_view_attr(wf, args, False))

    for wf in model.webtree:
        field_tree = {
            'name': wf.name,
            'width': str(wf.width),
            'component': wf.component,
        }
        webfields = data_view['webfields']
        if not webfields.get(wf.name):
            print('Warning: field not found: ', wf.name)
            continue

        field_tree.update(webfields.get(wf.name))
        if wf.color:
            try:
                field_tree['color'] = eval(wf.color)
            except:
                print('Warning: errado atributo de color para: ', wf.name)

        data_view['webtree'].append(field_tree)

    data_view['filters'] = {}
    for filter in model.filters:
        data_view['filters'][filter.name] = filter.domain

    return data_view
