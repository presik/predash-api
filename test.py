import requests
import simplejson as json

# The port is 5070 by default
# api_url = 'localhost:5070'
api_url = '0.0.0.0:5070'
database = 'DEMO60'

api = '/'.join(['http:/', api_url, database])
ctx = {
    'company': 1,
    'user': 1,
}
# 'shop': 1,  # id de Sucursal / Almacen / Tienda


def test1():
    args = {
        'party': {
            'id': 17,
            'name': 'JUAN MANUEL',  # optional
            'id_number': '13547322',  # optional
        },
        'shipment_date': '2021-05-17',
        'description': 'VENTA DE PRUEBA API',
        'lines': [
            {
                'product': 23,  # id of product
                'quantity': 1,  # integer / float
                'unit_price': 5000,
            }, {
                'product': 50,
                'quantity': 3,
                'unit_price': 3800,
            }
        ],
    }

    body = {
        'model': 'sale.sale',
        'method': 'dash_quote',
        'args': args,
        'context': ctx,
    }
    route = api + '/model_method'
    return route, body


def test2():
    body = {
        'model': 'party.party',
        'fields': ['id', 'id_number', 'name'],
        'args': {
            'domain': '[]',
        },
        'context': ctx,
        'order': [('name', 'ASC')]
    }
    route = api + '/search'
    return route, body


def test3():
    body = {
        'model': 'party.party',
        'context': ctx,
        'record': {
            'ext_id': '2123',
            'name': 'MARIO MANUEL',  # optional
            'id_number': '6993547322',  # optional
            'email': 'juan@gmail.com',
            'regime_tax': 'regimen_no_responsable',
            'address': 'CALLE 42 N 17 99',
            'type_document': '13', # Cedula de Ciudadania
            'type_person': 'persona_natural',
            'ciiu_code': '',
            'phone': '536284262',
            'city_code': '001',
            'country_code': '169',
            'department_code': '08',
            'birthday': '2010-10-03',
            'first_name': '',
            'second_name': '',
            'first_family_name': '',
            'second_family_name': '',
            'is_patient': False
        }
    }
    route = api + '/save_party'
    return route, body


def test4():
    body = {
        'model': 'laboratory.order',
        'context': ctx,
        'record': {
            'number': '00448',
            'customer': {
                'id_number': '123141',
                'ext_id': '144085',
                'is_person': True,
            },
            'patient': {
                'id_number': '123141',
                'ext_id': '144085',
                'is_person': True,
            },
            'description': '',
            'order_date': '2021-10-07',
            'company': 1,
            'list_price_code': '01',
            'operation_center': 1,
            'payment_term': 1,
            'discount_amount': 1500.56,
            'comment': 'ESTA ES UNA FACTURA DE PRUEBA CONLAB!',
            'contract': {
                'number': 'C000001',
                'category_code': '01',
            },
            'payments': [
                {
                    'kind': 'copago',
                    'payment_mode': '4',
                    'amount': '21443.4786',
                    'voucher': '00082627',
                },
            ],
            'lines': [
                {
                    'test_code': '167', # internal code
                    'quantity': '1',
                    'unit_price': '11417.1641', # max 4 digits
                }, {
                    'test_code': '156', # internal code
                    'quantity': '1',
                    'unit_price': '135129.1641', # max 4 digits
                },
            ]
        }
    }
    route = api + '/save_lab_order'
    return route, body


def test5():
    body = {
        'model': 'laboratory.test_type',
        'context': ctx,
        'record': {
            'ext_id': '2123',
            'name': 'UROANALISIS',  # optional
            'code': '43151',  # id_number
            'cups_code': '102987426',  # id_number
            'default_uom': 'u',  # id_number
            'category': '001',  # id_number
            'active': True,
            'description': '', # Alternate name
        }
    }
    route = api + '/save_test_type'
    return route, body


if __name__ == "__main__":
    # This method create a Sale Order (Este metodo crea una venta)

    route, body = test2()
    data = json.dumps(body)
    result = requests.post(route, data=data)
    print(result)
    values = result.json()
    if isinstance(values, dict):
        for k, v in values.items():
            print(k, ' : ', v)
    else:
        for v in values:
            print('---------------------------------')
            print(v)
